
## To Run It

1. Clone the project.
2. Run `npm install` or `yarn`
3. Run `npm start` or `yarn start`

Tests are available as `npm test` or `yarn test`.
