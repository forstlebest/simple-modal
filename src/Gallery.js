import React, { Component } from "react";
import PropTypes from "prop-types";
import Modal from "./Modal";
import './gallery.css';

const HOSTURL = 'https://rankss.herokuapp.com';

class Gallery extends Component {
  constructor(props) {
    super(props);
    console.log('PROPS:', props.attachments);
    this.state = {
      images: props.attachments,
      selectedImage: props.attachments.length !== 0 ? props.attachments[0].image.url : '',
      isOpen: false,
      modalState: false,
      attachments: [],
      image: {}
    };
    this.toggleModal = this.toggleModal.bind(this);
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.attachments !== nextProps.attachments) {
      this.setState({ selectedImage: nextProps.attachments[0].image.url })
    }
  }


  handleThumbClick(selectedImage) {
    this.setState({
      selectedImage
    });
  }

  toggleModal() {
    this.setState(({ isOpen }) => ({ isOpen: !isOpen }))
  }

  render() {
    const { images, isOpen, selectedImage } = this.state;
    const { attachments } = this.props;
    return (
      <div className="image-gallery">
        <div className="gallery-image">
          <div>
            <Modal
              onClose={this.toggleModal}
              show={isOpen}
              title="Example modal title"
            >
              <p>Lorjustoc venenatis nunc.. eros.</p>
              <p>
                Aliquam est dui,. turpis venenatis, mollis tincidunt tortor.
              </p>
              <img src={`${HOSTURL}${selectedImage}`} alt="Modal Image" style={{ width: '100%', height: '100%' }}/>
            </Modal>
            <div onClick={this.toggleModal}>
              <img src={`${HOSTURL}${selectedImage}`} />
            </div>
          </div>
        </div>
        <div className="image-scroller">
          {attachments.map((attach, index) => (
            <div key={index} onClick={this.handleThumbClick.bind(this, attach.image.url)}>
              <img src={`${HOSTURL}${attach.image.url}`} />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Gallery;
