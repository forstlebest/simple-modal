import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    if (!this.props.show) {
      return null;
    }

    return (
      <div className="modal is-active">
        <div className="modal-background" onClick={this.props.onClose} />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Image</p>
            <button className="delete" onClick={this.props.onClose} />
          </header>
          <section className="modal-card-body">
            <div className="content">
              {this.props.children}
            </div>
          </section>
          <footer className="modal-card-foot">
            <a className="button" onClick={this.props.onClose}>Close</a>
          </footer>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;
