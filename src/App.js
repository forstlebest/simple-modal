import React, { Component } from 'react';
import axios from 'axios'
import Modal from './Modal';
import Gallery from './Gallery';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { isOpen: false, modalImage: '', attachments: [] };
  }

  componentDidMount = () => {
    axios.get(`https://rankss.herokuapp.com/posts/7`)
      .then(res => {
        const { attachments } = res.data.post;
        this.setState({ attachments })
      })
  }


  openModal = (id) => {
    const { attachments } = this.state;
    this.setState({
      isOpen: !this.state.isOpen,
      modalImage: id !== undefined ? attachments.find(a => a.id === id).image.url : '',
    });
  }

  closeModal = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      modalImage: '',
    });
  }

  render() {
    const { attachments } = this.state;
    return (
      <div className="App">
        <Gallery attachments={attachments} />
      </div>
    );
  }
}

export default App;
